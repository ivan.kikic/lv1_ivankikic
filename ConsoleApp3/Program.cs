﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            // Test case //
            Zabiljeske kupnja = new Zabiljeske("Kupiti mlijeko");
            Zabiljeske putovanje = new Zabiljeske();
            Zabiljeske ispit = new Zabiljeske("Ispit iz Matematike u pon", 5);

            Console.WriteLine(kupnja.getTekst());
            Console.WriteLine(putovanje.getAutor());
            Console.WriteLine(ispit.getRazina());

            // Test ToString() metode //

            Console.WriteLine(ispit.ToString());

            // Test case // 
            VremenskaZabiljeska koncert = new VremenskaZabiljeska("Koncert u Osijeku", 4);
            VremenskaZabiljeska doktor = new VremenskaZabiljeska();

            Console.WriteLine(koncert.Tekst);
            Console.WriteLine(doktor.Vrijeme);

            koncert.Tekst = "Najbolji koncert ikada";

            Console.WriteLine(koncert.Tekst);

            // Test ToString() metode //

            Console.WriteLine(koncert.ToString());
            Console.WriteLine(doktor.ToString());
        }
    }
}
