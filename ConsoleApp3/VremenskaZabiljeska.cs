﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class VremenskaZabiljeska : Zabiljeske
    {
        // Dodatni atribut //
        private DateTime vrijeme;

        // Ctor //

        public VremenskaZabiljeska() : base()
        {
            this.vrijeme = DateTime.Now;
        }

        public VremenskaZabiljeska(string tekst, int razina) : base(tekst, razina) 
        {
            this.vrijeme = DateTime.Now;
        }

        public VremenskaZabiljeska(string tekst, int razina, DateTime vrijeme) : base(tekst, razina)
        {
            this.vrijeme = vrijeme;
        }

        // Override ToString() metoda

        public override string ToString()
        {
            return base.ToString() + "Vrijeme kreiranja: " + vrijeme.ToString() + ".\n";
        }

        // Svojstva

        public DateTime Vrijeme
        {
            get { return this.vrijeme; }
            set { this.vrijeme = value; }
        }
    }
}
