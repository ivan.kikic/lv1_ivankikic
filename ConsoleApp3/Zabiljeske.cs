﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Zabiljeske
    {
        // Atributi //
        private String tekst;
        private String autor;
        private int razina;

        // Geteri //
        public string getTekst() { return this.tekst; }
        public string getAutor() { return this.autor; }
        public int getRazina() { return this.razina; }

        // Seteri //
        public void setTekst(string tekst) { this.tekst = tekst; }
        public void setRazina(int razina) { this.razina = razina; }

        // Konstruktori //

        public Zabiljeske() // Defaultni ctor
        {
            this.tekst = " ";
            this.autor = "Ivan Kikic";
            this.razina = 0;
        }

        public Zabiljeske(string tekst) // Parametarski ctor
        {
            this.tekst = tekst;
            this.autor = "Ivan Kikic";
            this.razina = 1;
        }

        public Zabiljeske(string tekst, int razina) // Drugi parametarski ctor
        {
            this.tekst = tekst;
            this.autor = "Ivan Kikic";
            this.razina = razina;
        }

        // Svojstva 

        public string Autor
        {
            get { return this.autor; }
            private set { this.autor = value; }
        }

        public string Tekst
        {
            get { return this.tekst; }
            set { this.tekst = value; }
        }

        public int Razina
        {
            get { return this.razina; }
            set { this.razina = value; }
        }

        // Override ToString() metoda

        public override string ToString()
        {
            return "Autor zabiljeske je: " + this.autor + ".\nTekst zabiljeske je: "
                + this.tekst + ".\nRazina zabiljeske je: " + this.razina + ".\n";
        }
    }
}
